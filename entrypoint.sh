#!/bin/sh

if [ "$GROUP_ID" = "" ]; then GROUP_ID=$(id -g "$GROUP"); fi
if [ "$GROUP_ID" != "$(id -g "$GROUP")" ]; then
    find / -group "$GROUP" -exec chgrp "$GROUP_ID" {} \;
    groupmod --gid "$GROUP_ID" "$GROUP"
fi

if [ "$USER_ID" = "" ]; then USER_ID=$(id -u "$USER"); fi
if [ "$USER_ID" != "$(id -u "$USER")" ]; then
    find / -user "$USER" -exec chown "$USER_ID" {} \;
    usermod --uid "$USER_ID" "$USER"
fi

sed -i "/^;chdir/cchdir = $HOME/app" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^listen/clisten = 0.0.0.0:4324" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^;daemonize/cdaemonize = no" "/etc/php7/php-fpm.conf"
sed -i "/^;catch_workers_output/ccatch_workers_output = yes" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^;clear_env/cclear_env = no" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^;access.log/caccess.log = $HOME/log/php7.\$pool.access.log" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^;error_log/cerror_log = $HOME/log/php7.error.log" "/etc/php7/php-fpm.conf"
sed -i "/^;php_admin_value\[error_log\]/cphp_admin_value[error_log] = $HOME/log/php7.\$pool.error.log" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^;error_log/cerror_log = $HOME/log/php_errors.log" "/etc/php7/php.ini"
sed -i "/^group/cgroup = php" "/etc/php7/php-fpm.d/www.conf"
sed -i "/^user/cuser = php" "/etc/php7/php-fpm.d/www.conf"

find "$HOME" ! -group "$GROUP" -exec chgrp "$GROUP_ID" {} \;
find "$HOME" ! -user "$USER" -exec chown "$USER_ID" {} \;

exec su-exec "$USER" "$@"
