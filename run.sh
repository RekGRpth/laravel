#!/bin/sh

#docker build --tag rekgrpth/laravel . || exit $?
#docker push rekgrpth/laravel || exit $?
docker stop laravel
docker rm laravel
docker pull rekgrpth/laravel || exit $?
docker volume create laravel || exit $?
docker run \
    --add-host `hostname -f`:`ip -4 addr show docker0 | grep -oP 'inet \K[\d.]+'` \
    --detach \
    --env USER_ID=$(id -u) \
    --env GROUP_ID=$(id -g) \
    --hostname laravel \
    --name laravel \
    --publish 4324:4324 \
    --volume laravel:/data \
    rekgrpth/laravel
